/*
 * Copyright (C) 2018 the original author.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.*;

/**
 * This class is useful for converting between an array of values
 * for a set of given states and a single byte, as might be useful when
 * communicating with a micro-controller such as Arduino.
 *
 * @author Tom Cascio
 * @version 1.0
 */
public class DefaultSingleByteStateArrayConverter implements SingleByteStateArrayConverter {
  private final int[] format;
  private final int[] factors;

  /**
   * This constructor takes an array specifying the number of possible
   * values for each state.
   * <p>
   * For example: {3, 3, 2} would mean there are 3 possible values for
   * the first state, 3 for the second, and 2 for the third.
   *
   * @param format
   *     an array specifying the number of possible values for each state
   * @exception IllegalArgumentException
   *     if the there are problems with the format
   */
  public DefaultSingleByteStateArrayConverter(final int[] format) {
    // validate the format
    if (null == format) {
      throw new IllegalArgumentException("format cannot be null");
    }

    if (format.length < 1 || format.length > 8) {
      throw new IllegalArgumentException("format.length must be greater than 0 and less than 9");
    }

    // calculate factors
    this.factors = new int[format.length + 1];
    this.factors[0] = 1;
    for (int i = 0; i < format.length; i ++) {
      this.factors[i + 1] = this.factors[i] * format[i];
    }

    // validate factors
    if (this.factors[this.factors.length - 1] > 256) {
      throw new IllegalArgumentException("the total number of possible combinations required by the format cannot be greater than 256");
    }

    // clone format
    this.format = new int[format.length];
    System.arraycopy(format, 0, this.format, 0, format.length);
  }

  public short getStatesAsByte(final int[] states) {
    if (null == states) {
      throw new IllegalArgumentException("states cannot be null");
    }

    if (this.format.length != states.length) {
      throw new IllegalArgumentException("states.length must be equal to the length of the format");
    }

    short result = 0;
    for (int i = 0; i < states.length; i ++) {
      if (states[i] < 0 || states[i] >= this.format[i]) {
        throw new IllegalArgumentException("state values must be greater than or equal to zero and less than or equal to the max value according to the format");
      }
      result += states[i] * this.factors[i];
    }

    return result;
  }

  public int[] getByteAsStates(final short value) {
    if (value < 0) {
      throw new IllegalArgumentException("value cannot be less than zero");
    }
    
    // TODO: Additionally validate that the value doesn't exceed the max expected value
    // and that each state doesn't exceed its expected maximum

    int[] states = new int[this.format.length];
    for (int i = 0; i < states.length; i ++) {
      states[i] = (value % this.factors[i + 1]) / this.factors[i];
    }

    return states;
  }
}
