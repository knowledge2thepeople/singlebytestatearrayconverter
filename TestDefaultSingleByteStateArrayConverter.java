/*
 * Copyright (C) 2018 the original author.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.lang.annotation.*;
import java.lang.reflect.*;
import java.util.*;

public class TestDefaultSingleByteStateArrayConverter {

  public static void main(String[] args) throws Exception {
    for (Method testMethod : ReflectionUtil.findAnnotatedMethods(TestDefaultSingleByteStateArrayConverter.class, Test.class)) {
      testMethod.invoke(null);
    }
  }

  @Test
  public static void testFormatIsNull() {
    boolean thrown = false;
    try {
      SingleByteStateArrayConverter converter = new DefaultSingleByteStateArrayConverter(null);
    } catch (IllegalArgumentException exception) {
      thrown = true;
    }

    if (!thrown) {
      System.out.println(ReflectionUtil.getMethodName() + " failed: expected an IllegalArgumentException");
    } else {
      System.out.println(ReflectionUtil.getMethodName() + " passed");
    }
  }

  @Test
  public static void testFormatIsEmpty() {
    boolean thrown = false;
    try {
      int[] format = new int[0];
      SingleByteStateArrayConverter converter = new DefaultSingleByteStateArrayConverter(format);
    } catch (IllegalArgumentException exception) {
      thrown = true;
    }

    if (!thrown) {
      System.out.println(ReflectionUtil.getMethodName() + " failed: expected an IllegalArgumentException");
    } else {
      System.out.println(ReflectionUtil.getMethodName() + " passed");
    }
  }

  @Test
  public static void testFormatRequiresMoreThan8Fields() {
    boolean thrown = false;
    try {
      int[] format = new int[] {2, 2, 2, 2, 2, 2, 2, 2, 2};
      SingleByteStateArrayConverter converter = new DefaultSingleByteStateArrayConverter(format);
    } catch (IllegalArgumentException exception) {
      thrown = true;
    }

    if (!thrown) {
      System.out.println(ReflectionUtil.getMethodName() + " failed: expected an IllegalArgumentException");
    } else {
      System.out.println(ReflectionUtil.getMethodName() + " passed");
    }
  }

  @Test
  public static void testFormatCombinationsCannotBeRepresentedInASingleByte() {
    boolean thrown = false;
    try {
      int[] format = new int[] {3, 3, 3, 3, 3, 3};
      SingleByteStateArrayConverter converter = new DefaultSingleByteStateArrayConverter(format);
    } catch (IllegalArgumentException exception) {
      thrown = true;
    }

    if (!thrown) {
      System.out.println(ReflectionUtil.getMethodName() + " failed: expected an IllegalArgumentException");
    } else {
      System.out.println(ReflectionUtil.getMethodName() + " passed");
    }
  }

  @Test
  public static void testStateValueOutOfRange() {
    boolean thrown = false;
    try {
      int[] format = new int[] {3, 3, 3, 2, 2};
      SingleByteStateArrayConverter converter = new DefaultSingleByteStateArrayConverter(format);
      converter.getStatesAsByte(new int[] {0, 3, 2, 2, 1});
    } catch (IllegalArgumentException exception) {
      thrown = true;
    }

    if (!thrown) {
      System.out.println(ReflectionUtil.getMethodName() + " failed: expected an IllegalArgumentException");
    } else {
      System.out.println(ReflectionUtil.getMethodName() + " passed");
    }
  }

  @Test
  public static void test8DigitBinary() {
    int[] format = new int[] {2, 2, 2, 2, 2, 2, 2, 2};
    SingleByteStateArrayConverter converter = new DefaultSingleByteStateArrayConverter(format);

    short result = converter.getStatesAsByte(new int[] {1, 1, 1, 1, 1, 0, 1, 0});

    int expected = 95;
    if (result != expected) {
      System.out.println(String.format(ReflectionUtil.getMethodName() + " failed: expected %d was %d", expected, result));
    } else {
      System.out.println(ReflectionUtil.getMethodName() + " passed");
    }
  }

  @Test
  public static void test5DigitBase3() {
    int[] format = new int[] {3, 3, 3, 3, 3};
    SingleByteStateArrayConverter converter = new DefaultSingleByteStateArrayConverter(format);
    short result = converter.getStatesAsByte(new int[] {0, 1, 2, 2, 1});

    int expected = 156;
    if (result != expected) {
      System.out.println(String.format(ReflectionUtil.getMethodName() + " failed: expected %d was %d", expected, result));
    } else {
      System.out.println(ReflectionUtil.getMethodName() + " passed");
    }
  }

  @Test
  public static void testMixed() {
    int[] format = new int[] {3, 3, 3, 2, 2};
    SingleByteStateArrayConverter converter = new DefaultSingleByteStateArrayConverter(format);
    int[] states = new int[] {2, 1, 0, 1, 0};
    short result = converter.getStatesAsByte(states);
    int[] resultStates = converter.getByteAsStates(result);

    if (!Arrays.equals(resultStates, states)) {
      System.out.println(String.format(ReflectionUtil.getMethodName() + " failed: expected %s was %s", Arrays.toString(states), Arrays.toString(resultStates)));
    } else {
      System.out.println(ReflectionUtil.getMethodName() + " passed");
    }
  }

  @Retention(RetentionPolicy.RUNTIME)
  private @interface Test {}

  private static class ReflectionUtil {

    private static String getMethodName() {
      return Thread.currentThread().getStackTrace()[3].getMethodName();
    }

    private static List<Method> findAnnotatedMethods(Class<?> clazz, Class<? extends Annotation> annotationClass) {
      Method[] methods = clazz.getDeclaredMethods();
      List<Method> annotatedMethods = new ArrayList<Method>(methods.length);
      for (Method method : methods) {
        if (method.isAnnotationPresent(annotationClass)) {
          annotatedMethods.add(method);
        }
      }
      return annotatedMethods;
    }
  }
}
